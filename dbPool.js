const { Pool } = require("pg");

const pool = new Pool({
  user: "acropole_user",
  host: "localhost",
  database: "nova_acropole",
  password: "acropole_password"
});

module.exports = pool;
 