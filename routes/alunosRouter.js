const express    = require("express");
const router     = express.Router();

const AlunoModel = require("../models/AlunoModel");
const aux = require("../aux/aux");

const verificaData = date => {
  const age = aux.getAge(date);
  if (age < 0 || age > 120) {
    return "A idade do aluno não pode ser negativa e deve ser no máximo 120";
  }
  if (!aux.isDateValid(date)) {
    return "A data inserida não existe";
  }
  return "";
}

router.get("/", (req, res) => {
  // exibe a lista de alunos
  AlunoModel.getAll()
    .then(alunos => {
      res.render("alunos", {
        alunos: alunos,
        user: req.session.user
      });
    })
    .catch(err => {
      console.log("ERROR IS", err);
    });
});

router.get("/adicionar", (req, res) => {
  // exibe o formulario apra criar um novo aluno

  res.render("criarAluno", {
    user: req.session.user
  });
});

router.get("/:id", async (req, res) => {
  // visualizar um aluno específico

  const aluno = await AlunoModel.fromId(req.params.id);

  const presencas = await aluno.getPresencas();

  res.render("aluno", {
    aluno: aluno,
    presencas: presencas,
    user: req.session.user,
    credencial: req.session.user.credencial
  });

});

router.post("/", async (req, res) => {
  // adiciona um novo aluno

  console.log("recebi uma requisicao para criar um novo aluno");
  console.log(req.body);

  const date = aux.unformatDate(req.body.data_nascimento);
  
  const dataMessage = verificaData(date);
  if (dataMessage) {
    res.render("warning", {
      message: dataMessage,
      back: "/alunos/adicionar"
    });
    return;
  }

  if(req.body.nome.trim().split(" ").length <= 1) {
    res.render("warning", {
      message: "Insira o nome completo do aluno",
      back: "/alunos/adicionar"
    });
    return;
  }

  const aluno = new AlunoModel(
    req.body.nome, 
    req.body.email, 
    req.body.data_nascimento, 
    req.body.telefone, 
    req.body.nivel, 
    req.body.mensalidade
  );
  
  const isNameUnique = await aluno.isNameUnique();
  const isEmailUnique = await aluno.isEmailUnique();

  if (!isNameUnique) {
    console.log("O NOME DO ALUNO NÃO É ÚNICO");
    res.render("warning", {
      message: "Já existe um aluno com este nome!",
      back: "/alunos/adicionar"
    });
    return false;
  }
  if (!isEmailUnique) {
    console.log("O EMAIL DO ALUNO NÃO É ÚNICO");
    res.render("warning", {
      message: "Já existe um aluno com este E-mail!",
      back: "/alunos/adicionar",
      user: req.session.user
    });
    return false;
  }

  aluno.save()
    .then(id => {
      res.redirect(`/alunos/${[id]}`);
    });
});

router.put("/:id", async (req, res) => {
  // atualiza um aluno existente
  try {

    const aluno = await AlunoModel.fromId(req.params.id);

    const isEmailUnique = await (new AlunoModel("", req.body.email, "", "", "", "")).isEmailUnique();

    if (!isEmailUnique && req.body.email !== aluno.email) {
      res.render("warning", {
        message: "Este E-mail já está sendo utilizado por outro aluno!",
        back: `/alunos/${req.params.id}`
      });
      return;
    }

    const date = aux.unformatDate(req.body.data_nascimento);
    
    if (!aux.isDateValid(date)) {
      res.render("warning", {
        message: "A data de aniversário inseridade não existe",
        back: "/alunos/adicionar"
      });
      return;
    }

    await aluno.update(
      req.body.email, 
      req.body.data_nascimento, 
      req.body.telefone, 
      req.body.nivel, 
      req.session.user.credencial > 0 ? req.body.mensalidade : aluno.mensalidade
    );
    res.redirect(`/alunos/${req.params.id}`);
  } catch (e) {
    console.log("ERROR WHILE UPDATING ALUNO");
    console.log(e);
    res.redirect(`/alunos/${req.params.id}`);
  }
});

module.exports = router;