const express = require("express");
const router = express.Router();
const _ = require("lodash");
const aux = require("../aux/aux");

const TurmaModel = require("../models/TurmaModel");

router.use((req, res, next) => {
  if (!req.session.user || req.session.user.credencial < 2) {
    res.redirect("/");
  } else {
    next();
  }
});

router.get("/adicionar", (req, res) => {
  // exibe o formulario apra criar uma nova turma
  
  res.render("criarTurma", {
    user: req.session.user
  });
});

router.get("/:id", async (req, res) => {
  // visualizar uma turma específica

  const turma = await TurmaModel.fromId(req.params.id);
  const alunos = await turma.getAlunos();
  const presencas = await turma.getPresencas();

  const hoje = aux.formatDate(new Date());

  res.render("turma", {
    turma: turma,
    alunos: alunos,
    presencas: presencas,
    hoje: hoje,
    user: req.session.user
  });
});

router.get("/", (req, res) => {
  // exibe a lista de turmas

  TurmaModel.getAll()
    .then(turmas => {
      res.render("turmas", {
        turmas: turmas,
        user: req.session.user
      })
    });
});

router.post("/:id/alunos", (req, res) => {
  TurmaModel.fromId(req.params.id)
    .then(turma => {
      turma.addAlunoByName(req.body.nome)
        .then(val => {
          console.log("yeeehaw", val);
          if (val) res.redirect(`/turmas/${req.params.id}`);
          else res.render("warning", {
            message: "O aluno que você tentou adicionar à turma não existe",
            back: `/turmas/${req.params.id}`
          });
        })
        .catch(err => {
        });
    });
});

router.delete("/:id_turma/alunos/:id_aluno", (req, res) => {
  TurmaModel.fromId(req.params.id_turma).then(turma => {
    turma.removeAlunoById(req.params.id_aluno).then(a => {
      res.redirect(`/turmas/${req.params.id_turma}`);
    });
  }).catch(err => {
    res.redirect("/turmas/");
  });
});

router.post("/:id/presenca", async (req, res) => {
  const turma = await TurmaModel.fromId(req.params.id);

  console.log("fazer chamada:");
  console.log("turma", turma);
  console.log(req.body);

  const alunos = await turma.getAlunos();
  const alunosId = alunos.map(aluno => aluno.id);
  const presenteId = req.body.alunos ? 
    Object.keys(req.body.alunos).map(id => parseInt(id.replace("'", "")))
    : [];
  const faltouId = _.difference(alunosId, presenteId);
  const data = req.body.data_presenca;

  console.log("Alunos id", alunosId);
  console.log("Presente id", presenteId);
  console.log("Faltou id", faltouId);
  console.log("Data é", data);

  const date = aux.unformatDate(data);
  if (!aux.isDateValid(date)) {
    res.render("warning", {
      message: "A data inserida é inválida",
      back: `/turmas/${req.params.id}`
    });
    return;
  }

  // presenteId.forEach(async id => {
  //   await turma.addPresenca(id, data, true);
  // });
  // faltouId.forEach(async id => {
  //   await turma.addPresenca(id, data, false);
  // });

  for (let i = 0; i < presenteId.length; i++) {
    console.log(`O aluno de id ${presenteId[i]} veio na aula`);
    await turma.addPresenca(presenteId[i], data, true);
  }
  for (let i = 0; i < faltouId.length; i++) {
    console.log(`O aluno de id ${faltouId[i]} faltou`);
    await turma.addPresenca(faltouId[i], data, false);
  }

  res.redirect(`/turmas/${req.params.id}`);
});

router.post("/", async (req, res) => {
  // adiciona uma nova turma

  const turma = new TurmaModel(req.body.nome, req.body.horario);

  const isNameUnique = await turma.isNameUnique();

  if (!isNameUnique) {
    res.render("warning", {
      message: "Já existe uma turma com este nome!",
      back: "/turmas/adicionar"
    });
    return;
  }

  turma.save()
    .then(id => {
      res.redirect(`/turmas/${id}`);
    });
});

router.put("/:id", async (req, res) => {
  // atualiza uma turma existente

  const turma = await TurmaModel.fromId(req.params.id);
  await turma.update(req.body.horario_aula);

  res.redirect(`/turmas/${req.params.id}`);
});

module.exports = router;