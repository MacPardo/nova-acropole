const express = require("express");

const alunosRouter             = require("./alunosRouter");
const turmasRouter             = require("./turmasRouter");
const usuariosRouter           = require("./usersRouter");

const router = express.Router();
router.use("/login/", usuariosRouter);

router.use((req, res, next) => {
  if (!req.session.user) {
    res.redirect("/login");
  } else {
    next();
  }
})

router.use("/alunos/",   alunosRouter);
router.use("/turmas/",   turmasRouter);

router.get("/", function(req, res) {
  res.render("layout", {
    user: req.session.user
  });
});

module.exports = router;