const express = require("express");
const router = express.Router();

const UserModel = require("../models/UserModel");

router.get("/", (req, res) => {
  res.render("login", {
    user: req.session.user
  });
});

router.post("/", async (req, res) => {
  try {
    const user = await UserModel.fromPassword(req.body.senha);
  
    console.log("tentando logar no usuario", user);

    if (!user) {
      // res.redirect("/login");
      // return false;
      res.render("warning", {
        message: "Nome de usuário ou senha incorretos",
        back: "/login"
      });
    }

    req.session.user = user;
    res.redirect("/");

  } catch (e) {
    res.redirect("/login");
  }
});

router.delete("/", (req, res) => {
  req.session.user = undefined;
  res.redirect("/");
})

module.exports = router;