const _ = require("lodash");

const formatDate = date => {
  const d = new Date(date);

  let day = d.getDate().toString();
  let month = (d.getMonth() + 1).toString();
  let year = d.getFullYear().toString();

  if (day.length === 1) {
    day = `0${day}`;
  }
  if (month.length === 1) {
    month = `0${month}`;
  }

  return `${day}/${month}/${year}`;
}

const objectArrayToTable = (arr, rowName, colName, cellName, filler=" ") => {

  const table = {};

  const rows = _.union(arr.map(obj => obj[rowName]));
  const cols = _.union(arr.map(obj => obj[colName]));

  rows.forEach(row => {
    table[row] = {};
    cols.forEach(col => {
      table[row][col] = filler;
    })
  })

  arr.forEach(line => {
    const row = line[rowName];
    const col = line[colName];
    const cell = line[cellName];

    table[row][col] = cell;
  });

  return {
    table: table,
    rows: rows,
    cols: cols
  };
}

const unformatDate = dateStr => {
  const regex = /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/;
  const res = regex.exec(dateStr);

  const day = res[1];
  const month = res[2];
  const year = res[3];

  const date = new Date(`${year}-${month}-${day}`);

  return date;
}

const getAge = date => {
  const diff = (new Date()) - (new Date(date));
  const ageDate = new Date(diff);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const isDateValid = date => {
  return !isNaN(date.getDate());
}

module.exports = {
  formatDate: formatDate,
  objectArrayToTable: objectArrayToTable,
  unformatDate: unformatDate,
  getAge: getAge,
  isDateValid: isDateValid
}