const pool = require("../dbPool");
//const bcrypt = require("bcrypt");

class UserModel {
  constructor(nome_usuario, senha, credencial, id = undefined) {
    this.nome_usuario = nome_usuario;
    this.senha = senha;
    this.credencial = credencial;
    this.id = id;
  }

  static fromRow(row) {
    return new UserModel(row["nome_usuario"], "", row["credencial"], row["id_usuario"]);
  }

  static async fromId(id) {
    const query = "SELECT * FROM usuarios WHERE id_usuario = $1";
    const values = [id];

    const res = await pool.query(query, values);
    //return new UserModel(cols["nome_usuario"], undefined, cols["credencial"], id);
    return UserModel.fromRow(res.rows[0]);
  }

  static async fromPassword(password) {
    try {
      const query = "SELECT * FROM usuarios WHERE hash_senha = $1";
      const values = [password];
  
      const res = await pool.query(query, values);

      return UserModel.fromRow(res.rows[0]);

    } catch (e) {
      return undefined;
    }
  }

  isSaved() {
    return !!this.id;
  }

  async save() {
    try {
      if (this.isSaved()) return false;
      const query = `
        INSERT INTO usuarios
        (nome_usuario, hash_senha, credencial) VALUES
        ($1,           $2,         $3)
        RETURNING id_usuario
      `;
      const values = [this.nome_usuario, this.senha, this.credencial];
      const res = await pool.query(query, values);
  
      const id = res.rows[0].id_usuario;
      this.id = id;
      return id;
      
    } catch (e) {
      return undefined;
    }
  }

  static async setupTestingUsers() {
    try {
      const instrutor = new UserModel("instrutor", "instrutor", 2);
      const tesoureiro = new UserModel("tesoureiro", "tesoureiro", 1);
      const assistente = new UserModel("assistente", "assistente", 0);

      await instrutor.save();
      await tesoureiro.save();
      await assistente.save();
      return true;
    } catch (e) {
      console.log("error while trying to setup testing users", e);
      return false;
    }
  }

  async isNameUnique() {

    const query = "SELECT * FROM usuarios WHERE nome_usuario = $1";
    const values = [this.nome_usuario];

    const res = await pool.query(query, values);

    return res.rows.length === 0;
  }
}

module.exports = UserModel;