const pool = require("../dbPool");

const aux = require("../aux/aux");

class AlunoModel {

  constructor(nome, email, data_nascimento, telefone, nivel, mensalidade, id = undefined) {
    this.nome = nome;
    this.email = email;
    this.data_nascimento = data_nascimento;
    this.telefone = telefone;
    this.nivel = nivel;
    this.mensalidade = mensalidade;
    this.id = id;
  }

  isSaved() {
    return !!this.id;
  }

  static fromId(id) {
    const query = "SELECT * FROM alunos WHERE id_aluno=$1";
    const values = [id];
    return pool.query(query, values)
      .then(res => {
        const cols = res.rows[0];
        return this.fromRow(cols);
      });
  }

  static fromRow(row) {
    return new AlunoModel(
      row.nome_aluno,
      row.email,
      aux.formatDate(row.data_nascimento),
      row.telefone,
      row.nivel,
      row.mensalidade,
      row.id_aluno
    );
  }

  /*
   * returns Promise aluno_id
   */
  save() {

    const query = `
      INSERT INTO alunos 
      (nome_aluno, email, data_nascimento, telefone, nivel, mensalidade) VALUES
      ($1,         $2,    $3,              $4,       $5,    $6)
      RETURNING id_aluno
    `;
    const values = [this.nome, this.email, this.data_nascimento, this.telefone, this.nivel, this.mensalidade];

    return pool.query(query, values)
      .then(res => {
        const id_aluno = res.rows[0].id_aluno;
        this.id = id_aluno;
        return id_aluno;
      });
  }

  async update(email, data_nascimento, telefone, nivel, mensalidade) {
    try {
      const query = `
        UPDATE alunos 
        SET 
          email = $1, 
          data_nascimento = $2, 
          telefone = $3, 
          nivel = $4, 
          mensalidade = $5
        WHERE id_aluno = $6;
      `;
      const values = [email, data_nascimento, telefone, nivel, mensalidade, this.id];
      await pool.query(query, values);
      return true;
    } catch (e) {
      return false;
    }
  }

  async getPresencas() {
    const query = `
      SELECT * FROM presenca
        INNER JOIN turmas ON presenca.id_turma = turmas.id_turma
      WHERE presenca.id_aluno = $1
    `;
    const values = [this.id];
    const res = await pool.query(query, values);

    res.rows.forEach(row => {
      row["data_presenca"] = aux.formatDate(row["data_presenca"]);
    })

    return aux.objectArrayToTable(res.rows, "data_presenca", "nome_turma", "presente");
  }

  async isNameUnique() {
    try {
      if (this.isSaved()) return true;
  
      const query = `
        SELECT * FROM alunos WHERE nome_aluno = $1
      `;
      const values = [this.nome];
  
      const res = await pool.query(query, values);
      return res.rows.length === 0;
    } catch (e) {
      console.log("ERROR IN isNameUnique", e);
      return false;
    }
  }

  async isEmailUnique() {
    try {
      if (this.isSaved()) return true;

      const query = "SELECT * FROM alunos WHERE email = $1";
      const values = [this.email];

      const res = await pool.query(query, values);

      return res.rows.length === 0;
    } catch (e) {
      console.log("ERROR IN isEmailUnique", e);
      return false;
    }
  }

  static getAll() {
    const query = "SELECT * FROM alunos";
    const values = [];
    return pool.query(query, values)
      .then(res => {
        return res.rows.map(obj => {
          return this.fromRow(obj);
        });
      })
      .catch(err => {
        console.log("error: ", err);
        return [];
      });
  }

  static fromName(name) {
    console.log(`called AlunoModel.fromName("${name}")`)
    const query = "SELECT * FROM alunos WHERE nome_aluno=$1";
    const values = [name];
    return pool.query(query, values)
      .then(res => {
        const aluno = AlunoModel.fromRow(res.rows[0]);
        return aluno;
      });
  }
}

module.exports = AlunoModel;