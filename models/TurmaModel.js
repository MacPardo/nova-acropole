const pool = require("../dbPool");

const AlunoModel = require("./AlunoModel");
const aux = require("../aux/aux");

class TurmaModel {
  
  constructor(nome, horario, id = undefined) {
    this.nome = nome;
    this.horario = horario;
    this.id = id;
  }

  isSaved() {
    return !!this.id;
  }

  save() {
    const query = `
      INSERT INTO turmas
      (nome_turma, horario_aula) VALUES
      ($1        , $2) 
      RETURNING id_turma
    `;
    const values = [this.nome, this.horario];
    return pool.query(query, values)
      .then(res => {
        const id_turma = res.rows[0].id_turma;
        this.id = id_turma;
        return id_turma;
      });
  }

  async update(horario) {
    try {
      const query = `UPDATE turmas SET horario_aula = $1 WHERE id_turma = $2`;
      const values = [horario, this.id];
      await pool.query(query, values);
      return true;
    } catch (e) {
      return false;
    }
  }

  getAlunos() {
    // select * from alunos inner join aluno_turma on alunos.id_aluno = aluno_turma.id_aluno;
    const query = `
      SELECT * FROM alunos INNER JOIN aluno_turma 
      ON alunos.id_aluno = aluno_turma.id_aluno
      WHERE id_turma = $1
    `;
    const values = [this.id];
    return pool.query(query, values)
      .then(res => {
        return res.rows.map(obj => {
          return AlunoModel.fromRow(obj);
        });
      });
  }

  isAluno(idAluno) {
    const query = "SELECT * FROM aluno_turma WHERE id_aluno = $1 AND id_turma = $2";
    const values = [idAluno, this.id];
    return pool.query(query, values).then(res => {
      return res.rows.length > 0;
    }).catch(err => {
      return false;
    });
  }

  addAlunoByName(name) {
    return AlunoModel.fromName(name)
      .then(aluno => {
        console.log("The aluno I found is", aluno);
        const query = "INSERT INTO aluno_turma (id_aluno, id_turma) VALUES ($1, $2)";
        const values = [aluno.id, this.id];
        return pool.query(query, values)
          .then(res => {
            return true;
          }).catch(err => {
            return false;
          });
      })
      .catch(err => {
        console.log(`Could not find aluno with name (${name})`);
        return false;
      });
  }

  removeAlunoById(idAluno) {
    const query = "DELETE FROM aluno_turma WHERE id_aluno = $1 AND id_turma = $2";
    const values = [idAluno, this.id];
    return pool.query(query, values).then(res => {
      return true;
    }).catch(err => {
      return false;
    });
  }

  async addPresenca(idAluno, data, presente) {
    console.log("ENTERING addPresenca WITH VALUES ", idAluno, data, presente);
    try {
      const isAluno = await this.isAluno(idAluno);
      if (!isAluno) return false;
      
      const query = "INSERT INTO presenca (id_turma, id_aluno, data_presenca, presente) VALUES ($1, $2, $3, $4)";
      const values = [this.id, idAluno, data, presente ? "true" : "false"];

      const result = await pool.query(query, values);

      console.log("ADD PRESENCA COM VALORES", values);

      return true;
    } catch (e) {
      console.log("add presenca error:", e);
      return false;
    }
  }

  async getPresencas() {
    const query = `
      SELECT * FROM presenca 
        INNER JOIN alunos ON presenca.id_aluno = alunos.id_aluno 
        INNER JOIN turmas ON presenca.id_turma = turmas.id_turma 
      WHERE presenca.id_turma = $1
    `;
    const values = [this.id];

    const res = await pool.query(query, values);
    res.rows.forEach(row => {
      row["data_presenca"] = aux.formatDate(row["data_presenca"]);
    });

    const table = aux.objectArrayToTable(res.rows, "data_presenca", "nome_aluno", "presente");

    return table;
  }

  async isNameUnique() {
    try {
      if (this.isSaved()) return true;
  
      const query = "SELECT * FROM turmas WHERE nome_turma = $1";
      const values = [this.nome];
  
      const res = await pool.query(query, values);
      return res.rows.length === 0;

    } catch (e) {
      console.log("ERROR IN TurmaModel.isNameUnique", e);
      return false;
    }
  }

  static getAll() {
    const query = "SELECT * FROM turmas";
    const values = [];
    return pool.query(query, values)
      .then(res => {
        return res.rows.map(obj => {
          return new TurmaModel(
            obj.nome_turma,
            obj.horario_aula,
            obj.id_turma
          );
        });
      });
  }

  static fromId(id) {
    const query = "SELECT * FROM turmas WHERE id_turma=$1";
    const values = [id];
    return pool.query(query, values)
      .then(res => {
        const cols = res.rows[0];
        return new TurmaModel(
          cols.nome_turma,
          cols.horario_aula,
          cols.id_turma
        );
      });
  }
}

module.exports = TurmaModel;