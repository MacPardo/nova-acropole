const express        = require("express");
const app            = express();
const bodyParser     = require("body-parser");
const methodOverride = require("method-override");
const session        = require("express-session");

const mainRouter = require("./routes/routes");

const UserModel = require("./models/UserModel");

const aux = require("./aux/aux");

app.use(bodyParser.urlencoded({"extended": true}));
app.use(methodOverride("_method"));
app.set("view engine", "pug");
app.use(session({secret: "nova_acropole"}));

app.use("/bulma", express.static(__dirname + "/node_modules/bulma/css/"));
app.use("/public", express.static(__dirname + "/public"));

app.use(async (req, res, next) => {
  await UserModel.setupTestingUsers();

  const date = aux.unformatDate("10/10/2010");

  console.log("age diff", aux.getAge(date));

  next();
});

app.use(mainRouter);

const port = "8080";

app.listen(port, async () => {

  console.log(`everything is running on port ${port}`);
});


